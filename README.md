# CRUD-PERSONAS

Se realiza la creación del crud donde se ve la lista de personas, opciones de agregar, eliminar y editar

* El servicio se crea en el archivo que se encuentra en la raiz index.js
* La conexión a la base de datos se encuentra en la carpeta config/conexion.js
  se deben de cambiar la credenciales para la conexión a la base de datos
* La  rutas o setencias estan en la carpeta router/index.js

Para ejecutar el servicio se usa lo siguiente : npm run dev

