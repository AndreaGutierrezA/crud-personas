// inicializando la dependencia de posgres
const posgres = require('pg');

// conectarse a la base de datos
const { Pool } = require("pg");
//credenciales
const pool = new Pool({
    user: "postgres",
    host: "127.0.0.1",
    database: "BdPersonas",
    password: "3682",
    port: 5432,
});
pool.connect((err) => {
    if (err) {
        console.log('ha ocurrido un error ' + err);
    } else {
        console.log('la base de datos se conecto');
    }
});

// exportar la conexion para usarla desde cualquier parte del proyecto
module.exports = pool;