// se llama conexion
require('./config/conexion');
const cors = require("cors");
// instancia la dependencia de express
const express = require('express');
// configuración de puerto donde se conectara el servidor
// permite recuperar donde esta trabajando el servidor y si no existe entonces toma el puerto 3000
const port = (process.env.port || 3000);

// se llama la instancia de express y se guardar en app
const app = express();
// Lectura y parseo del body (para indicar que la data proveniente del cliente puede venir en JSON)
app.use(express.json());
// El propósito del CORS es eludir la medida de seguridad establecida como configuración predeterminada
app.use(cors());
// configurar puerto
app.set('port', port);

// para evitar problemas de conexion con angular
app.use('/api', require('./router/index'));

// iniciar express
app.listen(app.get('port'), (error) => {
    if (error) {
        console.log('error al inciar el servidor' + error);
    } else {
        console.log('servidor iniciado en el puerto ' + port);
    }
});