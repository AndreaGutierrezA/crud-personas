// acceder a router por medio de express
const router = require('express').Router();
// acceder a conexión
const conexion = require("../config/conexion");

// Realizar peticiones a la base de datos
// parametro de entrada y de salida
router.get('/', (req, res) => {
    let sql = 'select * from person';
    // se envia la consulta, parametros de error, fila y campos
    conexion.query(sql, (err, resp, fields) => {
        // si hay error
        if (err) throw err;
        // de lo contrario que muestre la consulta
        else {
            res.json(resp.rows);
        }
    });
});

// seleccionar una persona
router.get('/:id', (req, res) => {
    const { id } = req.params;
    let sql = 'SELECT * from person where id_person = $1';
    // se envia la consulta, parametros de error, fila y campos
    conexion.query(sql, [id], (err, rows, fields) => {

        if (err) throw err;

        else {
            res.json(rows);
        }
    });
    console.log(id);
});

//agregar persona
router.post('/', (req, res) => {
    const { identificacion, nombre, apellido, fechaNacimiento, sexo } = req.body;

    let sql = `INSERT INTO public.person(number_identification, full_name, full_last_name, birthdate, sex)
    values('${identificacion}','${nombre}','${apellido}','${fechaNacimiento}','${sexo}')`;
    conexion.query(sql, (err, rows, fields) => {
        if (err) throw err
        else {
            res.json({ status: 'Persona agregada' });
        }
    });
});

//eliminar 
router.delete('/:id', (req, res) => {
    const { id } = req.params

    let sql = `delete from person where id_person = '${id}'`;
    conexion.query(sql, (err, rows, fields) => {
        if (err) throw err
        else {
            res.json({ status: 'persona eliminada' });
        }
    });
});

//modificar
router.put('/:id', (req, res) => {
    const { id } = req.params;
    const { identificacion, nombre, apellido, fechaNacimiento, sexo } = req.body;

    let sql = `update person set 
                number_identification = '${identificacion}',
                full_name = '${nombre}',
                full_last_name = '${apellido}',
                birthdate = '${fechaNacimiento}',
                sex = '${sexo}'
                where id_person = '${id}'`;

    conexion.query(sql, (err, rows, fields) => {
        if (err) throw err
        else {
            res.json({ status: 'persona modificada' });
        }
    });

});
// exportar las rutas para usarla desde cualquier parte del proyecto
module.exports = router;