import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IPersona } from '../../interface';

@Component({
  selector: 'app-form-persona',
  templateUrl: './form-persona.component.html',
  styleUrls: ['./form-persona.component.scss']
})
export class FormPersonaComponent implements OnInit {

  @Input() data: IPersona;
  @Output() valid: EventEmitter<boolean> = new EventEmitter();
  @Output() validatedData: EventEmitter<IPersona> = new EventEmitter();

  validateForm: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.createForm();
    this.valid.emit(this.validateForm.valid);
    this.validateForm.valueChanges.subscribe((value) => {
      this.valid.emit(this.validateForm.valid);
      this.validateForm.valid
        ? this.validatedData.emit(value)
        : this.validatedData.emit(null);
    });
  }

  createForm(): void {
    this.validateForm = this.fb.group({
      id_person: [this.data === null ? null : this.data.id_person, []],
      identificacion: [this.data ? this.data.number_identification : '', [Validators.required]],
      nombre: [this.data ? this.data.full_name : '', [Validators.required]],
      apellido: [this.data ? this.data.full_last_name : '', [Validators.required]],
      fechaNacimiento: [this.data ? this.data.birthdate : '', [Validators.required]],
      sexo: [this.data ? this.data.sex : '', [Validators.required]],
    });
  }

}
