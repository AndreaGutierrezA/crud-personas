import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { IPersona } from '../interface';
import { PersonaService } from '../services/persona.service';

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.scss']
})
export class PersonaComponent implements OnInit {

  // modal
  isVisible = false;
  valid = false;
  validatedData;
  data;

  confirmModal?: NzModalRef;

  personas: IPersona[] = [];

  constructor(
    private modal: NzModalService,
    private personaService: PersonaService,
    private message: NzMessageService
  ) { }

  ngOnInit(): void {
    this.llenarPersona();
  }

  llenarPersona(): void {
    this.personaService.getPersonas().subscribe(
      (response) => {
        this.personas = response;
      },
      (error) => {
        this.message.create(error, `Se presento un error ${error}`);
      }
    );

  }

  // modal
  showModal(): void {
    this.isVisible = true;
  }

  isValid(event: any): void {
    this.valid = event;
  }

  getData(event: any): void {
    console.log(event);
    this.validatedData = event;
  }

  handleOk(): void {
    this.isVisible = false;
    this.valid = false;

    if (this.validatedData) {
      if (this.validatedData.id_person) {
        this.personaService
          .actualizarPersona(this.validatedData)
          .subscribe(
            (success) => {
              this.message.success('Registro modificado', {
                nzDuration: 1000
              });
              this.llenarPersona();
            },
            (error) => {
              this.message.create(error, `Se presento un error ${error}`);
            }
          );
      } else {
        this.personaService.agregarPersona(this.validatedData).subscribe(
          (success) => {
            this.message.success('Registro guardado', {
              nzDuration: 1000
            });
            this.llenarPersona();
          },
          (error) => {
            this.message.create(error, `Se presento un error ${error}`);
          }
        );
      }
    }
  }

  handleCancel(): void {
    this.isVisible = false;
    this.valid = false;
  }

  onCreate(): void {
    this.data = null;
    this.showModal();
  }
  onEditar(item: IPersona): void {
    console.log(item);
    this.data = item;
    this.showModal();
  }
  onDelete(item: IPersona): void {
    this.modal.confirm({
      nzTitle: 'Desea eliminar a la siguiente persona?',
      nzContent: `<b>Identificacion:</b> ${item.number_identification}<br> <b>Nombre:</b>  ${item.full_name} ${item.full_last_name} `,
      nzOkText: 'Si',
      nzOkType: 'primary',
      nzOkDanger: true,
      nzOnOk: () => {
        this.personaService.eliminarPersona(item).subscribe(
          (success) => {this.message.success('Registro eliminado', {
            nzDuration: 1000
          });
            this.llenarPersona();
          },
          (error) => {
            this.message.create(error, `Se presento un error ${error}`);
          }
        );
      },
      nzCancelText: 'No',
      nzOnCancel: () => console.log('Cancelar')
    });
  }

}
