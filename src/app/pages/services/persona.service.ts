import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IPersona } from '../interface';

@Injectable({
  providedIn: 'root'
})
export class PersonaService {
  private url: string = 'http://localhost:3000/api';

  constructor(private http: HttpClient) { }

  getPersonas(): Observable<IPersona[]> {
    return this.http.get<IPersona[]>(this.url);
  }

  agregarPersona(persona: IPersona): Observable<IPersona> {
    return this.http.post<IPersona>(this.url, persona);
  }
  actualizarPersona(persona: IPersona): Observable<IPersona> {
    return this.http.put<IPersona>(this.url + '/' + persona.id_person, persona);
  }
  eliminarPersona(persona: IPersona): Observable<IPersona> {
    return this.http.delete<IPersona>(this.url + '/' + persona.id_person);
  }
}