export interface IPersona {
    id_person?: string;
    number_identification: string;
    full_name: string;
    full_last_name: string;
    birthdate: string;
    sex: string;
  }
